echo "##### Importing data in MAP database for MongoDB TP"

mongoimport --db pau /vol/data/bar.js
mongoimport --db pau /vol/data/bus.js
mongoimport --db pau /vol/data/canisette.js
mongoimport --db pau /vol/data/corbeille.js
mongoimport --db pau /vol/data/fontaine.js
mongoimport --db pau /vol/data/horodateur.js
mongoimport --db pau /vol/data/idcycle.js
mongoimport --db pau /vol/data/parking.js
mongoimport --db pau /vol/data/parkingvelo.js
mongoimport --db pau /vol/data/pistecycle.js
mongoimport --db pau /vol/data/stationnement.js
mongoimport --db pau /vol/data/verre.js
