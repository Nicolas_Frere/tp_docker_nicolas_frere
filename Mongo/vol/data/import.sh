#!/bin/bash

while :
do
  (echo > /dev/tcp/mongodb/27017) 2> /dev/null
  result=$?
  echo "result=$result"
  if [[ $result -eq 0 ]]; then
    break
  fi
  sleep 1
done

mongo --host mongodb /vol/data/*.js
