Pour lancer le projet : 

docker-compose up --build

s'il y a une erreur lors de la création du network,
réessayer en coupant openvpn (service openvpn stop)
le relancer ensuite pour pouvoir effectuer la suite des commandes (service openvpn start)

Ouvrir un navigateur et ouvrir la page localhost:8888

Si tout fonctionne normalement, une carte de Pau s'affiche avec un marqueur au niveau de chaque bar de Pau

Les coordonnées des marqueurs viennent du container mongodb
